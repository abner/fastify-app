
import { FastifyInstance } from 'fastify';

// const fastify = fastifyCtor({
//     logger: {
//         level: 'info'
//     }
// });

const fastify: FastifyInstance = require('fastify')({
    logger: {
        level: 'info'
    }
});

const HTTP_PORT = parseInt(process.env['HTTP_PORT'] || '3033');

// const acceptsSerializer = require('fastify-accepts-serializer');

// console.log('ACCEPTS SERIALIZER', acceptsSerializer);

// try {

//     fastify.register(require('fastify-accepts-serializer'), {
//         serializers: [
//             {
//                 regex: /^application\/json$/,
//                 serializer: (body: any) => JSON.stringify(body)
//             }
//         ],
//         default: 'application/json' // mime type used if Accept header don't match anything
//     });
// } catch (e) {
//     console.error('ERROR', e);
// }


fastify.get('/', (req, reply) => {
    reply
        // .header('Content-Type', 'application/json')
        .send('Olá mundo');
});

fastify.listen(HTTP_PORT, (err) => {
    if (err) {
        console.error(err);
        throw err;
    }
    console.log(`Servidor Fastify inicializado com sucesso na porta ${HTTP_PORT}`);
});